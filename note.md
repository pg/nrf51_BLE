# Flashing nrf firmaware on OSX 10.8

```
sudo mount -u -w -o sync /Volumes/MBED
srec_cat  /Users/pg/code/blenano/s110_nrf51822_7.3.0_softdevice.hex -intel _build/ble_app_uart_s110_xxaa.hex -intel -o _build/uart.hex -intel --line-length=44
cp -X _build/uart.hex /Volumes/MBED/
sudo rm  /Library/Preferences/com.apple.Bluetooth.plist
pkill bluez
```

