ble_app_uart-example
==================

This project contains code examples that show nrf51 UART functionality with the app_uart library.
Instructions on how to operate the example is given at the top of the main file in the example.

 
Requirements
------------
- nRF51 SDK version 6.1.0
- S110 SoftDevice version 7.0.0
- nRF51822 Development Kit version 2.1.0 or later
- python 2.7 (3.x?) 
- PyBLEWrapper https://github.com/brettchien/PyBLEWrapper.git

To compile it, clone the repository in the \nrf51822\Board\nrf6310\S110\ folder.

Build and run
-------------

* Compile
```
cd gcc && make
```
* Flashing nrf firmaware on OSX 10.8

```
sudo mount -u -w -o sync /Volumes/MBED
srec_cat  /Users/pg/code/blenano/s110_nrf51822_7.3.0_softdevice.hex -intel _build/ble_app_uart_s110_xxaa.hex -intel -o _build/uart.hex -intel --line-length=44
cp -X _build/uart.hex /Volumes/MBED/
sudo rm  /Library/Preferences/com.apple.Bluetooth.plist
pkill bluez
```

* Run python script and monitor serial port
```
python python/test.py
```

```python

import sys
import serial
s=serial.Serial("/dev/tty.usbmodem1412")
while True : sys.stdout.write( s.read() )
```